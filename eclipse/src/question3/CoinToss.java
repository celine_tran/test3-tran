package question3;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/*Note: This question does NOT involve networking in any way. That is, you can and should code the whole task as a single program.

Your program should do the following:

�         The user always starts out with 100 units of money

�         The user can enter, via a TextField, the amount of money they wish to bet each round.

�         In each round, the user chooses whether to bet on heads or tails by hitting a button for heads or a button for tails. This button uses the above text field to get the amount bet in the round.

�         If the user has placed an invalid bet (e.g. they don�t have enough money, they didn�t enter a number, etc) then there should be no change. A message should be displayed to the user indicating the issue.

�         If the user has placed a valid bet, you should simulate a coin flip, which has a 50% chance of being heads and a 50% chance of being tails, using a random number. If the user is correct, they gain the money they bet. If they are wrong they lose the money they bet.

�         The program must display what happened in each round as well as how much money the user has. You may choose how you�d like to display this information.


Notes:

�         Your program will not be graded based off of how it looks, so don�t worry about the graphical design unless you have extra time

�         For full marks, your program should use the proper encapsulation and separation of concerns ideas we�ve discussed in class, on assignments, etc

�         Your code should follow the best practices of readability, data validation, etc as discussed in class. JavaDocs style formal comments are not required, but commenting is.

*/

public class CoinToss {

	public void start(Stage stage) {
        Group root = new Group();
        //create scene and associate to root
        Scene scene = new Scene(root, 450, 300);
        VBox overall = new VBox();
        HBox buttons =new HBox();
        
        //whats left of the money
        Text unitleft = new Text("100");
        //money they wish to input 
        TextField input = new TextField("Input bet here!");
        
        
        Button heads =new Button("heads");
        Button tails =new Button("tails");
        int headnum=1;
        int tailsnum=0;
        //event handlers initialization + calls
        HandleButtons headsEvent= new HandleButtons(unitleft,input,headnum);
        HandleButtons tailsEvent= new HandleButtons(unitleft,input,tailsnum);
        heads.setOnAction(headsEvent);
        tails.setOnAction(tailsEvent);
        
        buttons.getChildren().addAll(heads,tails);
        overall.getChildren().addAll(input,buttons,unitleft);
        root.getChildren().add(overall);
       
        //associate scene to stage and show
        scene.setFill(Color.YELLOW);
        stage.setTitle("Coin Toss");
        stage.setScene(scene);
        stage.show();

    }


    /**
     * This is the main method where we will call the launch to start the GUI.
     */
    public static void main(String[] args) {
        Application.launch(args);
    }

}

